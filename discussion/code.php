code.php<?php

// Repetition Control Structures

// Repetition control structures are used to execute code multiple times

// While Loop
// A while loop takes a single condition. If the condition evaluates to true, the code inside the block will run.

function whileLoop()
{
    $count = 5;

    while ($count !== 0) {
        echo $count . '<br/>';
        $count--;
    }
}

// Do-While Loop
// A do-while loop works a lot likethe while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

function doWhileLoop()
{
    $count = 20;

    do {
        echo $count . '<br/>';
        $count--;
    } while ($count > 20);
}

// For Loop 
/*  
    for(initialValue; condition; iteration) {
        // code block
    }
*/

function forLoop()
{
    for ($count = 0; $count <= 20; $count++) {
        echo $count . '<br/>';
    }
}

// Continue and Break Statements
// 

function modifiedForLoop()
{
    /* 
        "Continue" is a keyword that allows the code to go to the next loop without finishing the current code block.

        "Break" on the other hand is a keyword that STOP the execution of the current loop.
    */

    for ($count = 0; $count <= 20; $count++) {
        if ($count % 2 === 0) {
            continue;
        }

        echo $count . '<br/>';

        if ($count > 10) {
            break;
        }
    }
}

/* 
    Mini-Activity:
        The While Loop should only display the number 1 - 5. Correct the following loops.

        function whileLoopActivity() {
            $x = 0;

            while ($x < 1) {
                echo $x . '<br/>';
                $x++;
            }
        }
*/

function whileLoopActivity()
{
    $x = 1;

    while ($x <= 5) {
        echo $x . '<br/>';
        $x++;
    }
}

//Array Manipulation
// An array is a kind of variable that can hold more than one value.
//Arrays are declared using array() function or square bracket '[];
//In the early versions of php, we cannot used [], but as of PHP 5.4 we can used the short array syntax which replaces array() with [];

$studentNumbers = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //before PHP 5.4

$studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; //after PHP 5.4

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

$tasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];

// Associative Array
// Associative array differ from numberic array in the sense that associative array uses descriptive names in naming the elements/values (key => value pair).
// double arrow (=>) an assignment operator that is commonly used in the creation of associative array.

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-Dimensional Array

$heroes = [
    ['iron man', 'thor', 'hulk'],
    ['wolverine', 'cyclops', 'jean grey'],
    ['batman', 'superman', 'wonder woman']
];

// Two-Dimensional Associative Array
$ironManPowers = [
    'regular' => ['repulsor blast', 'rocket punch'],
    'signature' => ['unibeam']
];

// Array Methods
// Array Sorting
$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

// Ascending order
sort($sortedBrands);

// Descending order
rsort($reverseSortedBrands);

// Other Array Functions
// Search of an element

function searchBrand($brand, $brands)
{
    // in-array ($searchValue, $arrayList)
    return (in_array($brand, $brands)) ? "$brand is in the array" : "$brand is not in the array.";
}

// array_reverse()
// 
$reversedGradePeriods = array_reverse($gradePeriods);